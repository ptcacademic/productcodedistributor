# ProductCodeDistributor
A simple tool for the distribution of unique product codes which are line seperated in a txt file.

##How to Use

1.  Launch the PCDistrib.jar file
2.  Click browse
3.  Navigate to a .txt file containing product codes
4.  Click "Copy 1" to copy a single product code to the clipboard OR type a number into the "X = " box and then click on "Copy X"
5.  Paste to insert the copied product cdoes

##Product Code File Requirements

- Must be saved as a .txt file
- Each line must contain a single product code with no leading or trailing characters
- Cannot be longer than ~300 product codes
- (Optional) The default location that the "Browse... " button will open is the same location as the .jar file.  For easiest access save the Product Code file there
