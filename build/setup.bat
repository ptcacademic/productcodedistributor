echo on
mkdir %USERPROFILE%\Documents\ProductCodeDistributor
copy %CD%\* %USERPROFILE%\Documents\ProductCodeDistributor

@Echo off

rem Window Style
REM 1 = Normal, 3 Maximized, 7 = Minimized

rem Choose "Desktop" or "AllUsersDesktop"
set Location="Desktop"

set DisplayName="Product Code Distributor"
set filename="%USERPROFILE%\Documents\ProductCodeDistributor\PCDistrib.jar"

REM Set icon to an icon from an exe or "something.ico"
set icon="%USERPROFILE%\Documents\ProductCodeDistributor\Icon.ico, 0"

set WorkingDir="%USERPROFILE%\Documents\ProductCodeDistributor"

set Arguments=""

REM Make temporary VBS file to create shortcut
REM Then execute and delete it

(echo Dim DisplayName,Location,Path,shell,link
echo Set shell = CreateObject^("WScript.shell"^)
echo path = shell.SpecialFolders^(%Location%^)
echo Set link = shell.CreateShortcut^(path ^& "\" ^& %DisplayName% ^& ".lnk"^)

echo link.Description = %DisplayName%
echo link.TargetPath = %filename%
echo link.Arguments = %arguments%

echo link.WindowStyle = 7
echo link.IconLocation = %icon%

echo link.WorkingDirectory = %WorkingDir%
echo link.Save

)> "%temp%\makelink.vbs"
cscript //nologo "%temp%\makelink.vbs"
del "%temp%\makelink.vbs" 2>NUL


rem Window Style
REM 1 = Normal, 3 Maximized, 7 = Minimized

rem Choose "Desktop" or "AllUsersDesktop"
set Location="StartMenu"

set DisplayName="Product Code Distributor"
set filename="%USERPROFILE%\Documents\ProductCodeDistributor\PCDistrib.jar"

REM Set icon to an icon from an exe or "something.ico"
set icon="%USERPROFILE%\Documents\ProductCodeDistributor\Icon.ico, 0"

set WorkingDir="%USERPROFILE%\Documents\ProductCodeDistributor"

set Arguments=""

REM Make temporary VBS file to create shortcut
REM Then execute and delete it

(echo Dim DisplayName,Location,Path,shell,link
echo Set shell = CreateObject^("WScript.shell"^)
echo path = shell.SpecialFolders^(%Location%^)
echo Set link = shell.CreateShortcut^(path ^& "\" ^& %DisplayName% ^& ".lnk"^)

echo link.Description = %DisplayName%
echo link.TargetPath = %filename%
echo link.Arguments = %arguments%

echo link.WindowStyle = 7
echo link.IconLocation = %icon%

echo link.WorkingDirectory = %WorkingDir%
echo link.Save

)> "%temp%\makelink.vbs"
cscript //nologo "%temp%\makelink.vbs"
del "%temp%\makelink.vbs" 2>NUL


