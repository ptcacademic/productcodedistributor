/*
 * Copyright (C) 2013 Mark Cheli
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pcdistrib;
/*
Program PCDistrib:  Reads the text file "productcodes", displays the next product code in the file, deletes said product code, then re-saves the file
By Mark Cheli
12/5/13
*/
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class writer {
    
    //Takes the activecodes array and deletes the values that have already been copied onto the clipboard from the file
    public void write(int numbercodes, File file) throws FileNotFoundException, IOException
    {        
        Scanner fileScanner = new Scanner(file);  //Creates a new scanner that will use the file as the input
        
        while(numbercodes != 0) //Loops the number of times equal to the number of product codes selected
        {
            fileScanner.nextLine(); //Skips a line in the file document
            numbercodes--; //Decreases the value of nubmercodes by 1
        }
        
        FileWriter fileStream = new FileWriter(file); //Creates a new filewriter called filestream
        BufferedWriter out = new BufferedWriter(fileStream); //Creates a new bufferedwriter called out
        
        //Re-writes the document without the first few lines (Where the first few liens are the number of product codes selected)
        while(fileScanner.hasNextLine())  //Loops when there is still lines left in the document
        {
            String next = fileScanner.nextLine(); //Sets the string (next) equal to teh next line in the document
            if(next.equals("\n")) //Checks to see if the next line is a new line character
            {
                out.newLine(); //Creates a new line
            }
            else //For all other instances
            {
                out.write(next);  //Writes the next line
            }
            out.newLine();   //Writes a line
        }
        out.close(); //Writes a line
    }
    
    //WRites a new file path onto the filehistory file when a new file is opened
     public static void lastfile(File recentfile) throws IOException 
     {
        String filehistorypath = "filehistory.txt"; //Sets the string of filehistorypath equal to the filepath of the 
        File filehistory = new File(filehistorypath); //Sets the file filehistory equal to the filepath specified above

        FileWriter fileStream = new FileWriter(filehistory); //Creates a filewriter that will be able to wrrite to the file
        try (BufferedWriter out = new BufferedWriter(fileStream))  //Trys to create a bufferedwriter that will write ot the file
        {
            out.append(recentfile+"\n"); //Writes the most recent file to the file (recentfile)
        }

    }
}