/*
 * Copyright (C) 2013 Mark Cheli
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pcdistrib;
/*
Program PCDistrib:  Reads the text file "productcodes", displays the next product code in the file, deletes said product code, then re-saves the file
By Mark Cheli
12/5/13
*/
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class reader
{
    //Puts the number of product code that the user requested in an array (activecode[])
    public String[] getcodes(int numbercodes, File file) throws FileNotFoundException, IOException 
    {      
        String charset = "UTF-8"; //Changes the string charset to UTF-8
        String[] activecode; //Creates an array activecode that will store the product codes
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), charset));   //Creates a new buffered filereader that uses file as the input
            Scanner fin = new Scanner(file))  //Creates a new scanner that uses file as the input
        {
            activecode = new String[numbercodes]; //Sets activecode equal to the a strang array that has a length equal to the number of codes being selected
            int counter = 0; //Sets the counter equal to 0
            try {
                while (counter != activecode.length) //Executes while the counter is not equal to the length of the activecode array
                {
                    activecode[counter] = fin.next(); //Sets an element in the activecode array at the location specified by the counter number as the next available product code
                    counter++; //Increases the counter by 1
                }
            } catch (NoSuchElementException e) {
            }
        }

        return activecode;  //Returns the value of activecode
    }
    //Reads what the last file was used so that when the GUI starts, the last file can be loaded
    public String lastfile() 
    {
        String filehistorypath = "D:\\Users\\mcheli\\Documents\\GitHub\\ProductCodeDistributor\\bin\\filehistory.txt"; //Sets a string with the path of the filehistory file
        File filehistory = new File(filehistorypath); //Sets the file filehistory equal to the filepath set by the filehistorypath string
        
        Scanner fileScanner = null; //Sets the filescanner equal to null
        try {
            fileScanner = new Scanner(filehistory); //Scanns the filehistory file
        } catch (FileNotFoundException ex) {
            Logger.getLogger(reader.class.getName()).log(Level.SEVERE, null, ex);
        }
        String lastfile = fileScanner.nextLine(); //Creates a string that has the finformation about what was the last file
        
        if("New".equals(lastfile)) //Runs the next statement if the filehistory file has a line named New
        {
            return null;  //Returns null
        }

        return lastfile;  //Returns the filehistory path if it is not named new
    }

}