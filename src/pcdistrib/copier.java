/*
 * Copyright (C) 2013 Mark Cheli
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pcdistrib;
/*
Program PCDistrib:  Reads the text file "productcodes", displays the next product code in the file, deletes said product code, then re-saves the file
By Mark Cheli
12/5/13
*/
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

public class Copier 
{  
    //Takes the activecode array that was assigned in the reader class and puts the information in the system clipboard
    void copy(String[] activecode)
    {    
        //Initiates the value of the String productcode by readering the first value of the array activecode
        String productcode = activecode[0];
        
        //Sets the value of counter equal to 1 (since the first value of the array has been used), then runs a loop that adds a product code to the next line of the string productcode for the number of items in the array
        int counter = 1;
        while(counter != activecode.length)
        {
            productcode = productcode + "\n" + activecode[counter];
            counter++;
        }
        
        //Creates a selection of the full product string
        StringSelection selection = new StringSelection(productcode);
        
        //Copies the full product string onto the system clipboard
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(selection, selection);
    }
}
