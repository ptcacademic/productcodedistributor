/*
 * Copyright (C) 2013 Mark Cheli
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pcdistrib;
import java.awt.Dimension;
/*
Program PCDistrib:  Reads the text file "productcodes", displays the next product code in the file, deletes said product code, then re-saves the file
By Mark Cheli
12/5/13
*/
import java.awt.Toolkit;
import java.io.IOException;
import javax.swing.JFrame;

public class PCDistrib {

    public static void main(String[] args) throws IOException
        {
            JFrame primaryWindow = new JFrame("Product Code Distributor"); //Creates a new Java Window Frame named best that displays "Product Code Distributor" as the title
            
            primaryWindow.add(new PCDistribGUI()); //Adds the GUI to the Frame
            primaryWindow.setIconImage(Toolkit.getDefaultToolkit().getImage("Icon.png"));  //Sets the Icon for the window
            primaryWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Sets the default exit operation to close the window
            
            //Display the window.
            primaryWindow.pack();
            primaryWindow.setVisible(true);
            
            //Set properties for the window
            primaryWindow.setResizable(false);
			primaryWindow.setSize(800, 75);
			Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
			primaryWindow.setLocation(dim.width/3-primaryWindow.getSize().width/2, dim.height/3-primaryWindow.getSize().height/2);

    }
    
}