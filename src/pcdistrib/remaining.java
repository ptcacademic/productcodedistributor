/*
 * Copyright (C) 2013 Mark Cheli
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package pcdistrib;
/*
Program PCDistrib:  Reads the text file "productcodes", displays the next product code in the file, deletes said product code, then re-saves the file
By Mark Cheli
12/5/13
*/
//This a test of the VCS
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class remaining {
    
    public String remaining(File file) throws UnsupportedEncodingException, FileNotFoundException, IOException
    {
        File productcodes = file; //Loads the file productcodes for reading

        String charset = "UTF-8"; //Changes the string charset to UTF-8

        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(productcodes), charset)); //Creates a buffered reader for the productcodes file
        
        String linereader; //Instanciates linereader
        int remaining = 0; //Sets the value of remaining to 0
        while((linereader = reader.readLine()) != null)  //while the string linereader(which is equal to the nextline in the document) is not equal to null
        { 
            remaining++; //Increases remaining by one
        }
        
        String output = ""+remaining; //Creates a string output that has the nubmer of product codes that are remaining
        
        return output; //Returns the number of product codes that are remaining
    }
}
