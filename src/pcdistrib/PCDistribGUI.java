/*
 * Copyright (C) 2013 Mark Cheli
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pcdistrib;
/*
Program PCDistrib:  Reads the text file "productcodes", displays the next product code in the file, deletes said product code, then re-saves the file
By Mark Cheli
12/5/13
*/
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

public class PCDistribGUI extends javax.swing.JPanel implements ActionListener 
{
    ;
    File file = null; //Sets the value of file to null initially.  This prevents an incorrect file from being selected
    private JButton copyButton;  //Creates a button on the GUI
    private JButton copyButtonX;  //Creates a button on the GUI
    private JFileChooser chooser;  //Creates a java file chooser
    private JButton browseButton;  //Creates a button on the GUI
    private JTextField filelocation;  //Creates a text field
    private JTextField numbercodesText; //Creates a text field
    private JTextField remaining;  //Creates a text field
    private JLabel remaininglabel;  //Creates a label
    private JLabel X;  //Creates a label

    static private final String newline = "\n"; //Sets teh string newline equal to the newline character.  To be used as an easier way to create a new line

    //A public method that will initiate the GUI Components
    public PCDistribGUI() 
    {
        initComponents(); //Initiates all of the GUI components
    }

    //Private method to initiate the GUI Components
    private void initComponents() 
    {
        filelocation = new JTextField();
        filelocation.setBounds(111, 14, 267, 22);
        copyButton = new JButton();  //Creates a button on the GUI
        copyButton.setBounds(529, 13, 100, 25);
        copyButtonX = new JButton();  //Creates a button on the GUI
        copyButtonX.setBounds(715, 13, 73, 25);
        browseButton = new JButton();  //Creates a button on the GUI
        browseButton.setBounds(12, 13, 87, 25);
        chooser = new JFileChooser(System.getProperty("user.dir"));  //Creates a java file chooser
        numbercodesText = new JTextField();  //Creates a text field
        numbercodesText.setBounds(674, 14, 29, 22);
        remaining = new JTextField();  //Creates a text field
        remaining.setBounds(467, 14, 50, 22);
        remaininglabel = new JLabel("Remaining:");  //Creates a label
        remaininglabel.setBounds(390, 17, 65, 16);
        X = new JLabel("X =");  //Creates a label
        X.setBounds(641, 17, 21, 16);

        filelocation.setColumns(15);  //Sets the number of columns in filelocation to 15
        filelocation.setEditable(false);  //Sets the textfield to not editable

        FileNameExtensionFilter filter = new FileNameExtensionFilter("TEXT FILES", "txt", "text");  //Creates a filter for the filechooser.  Only allows text files to be selected  
        chooser.setFileFilter(filter);  //Applies the filter to the filechooser
        chooser.addActionListener(this);  //Associates the actionlistener with the file chooser GUI component

        browseButton.setText("Browse...");  //sets the name of the browsebutton to "Browse..."
        browseButton.addActionListener(this); //Associates the actionlistener with the browse button GUI component

        copyButton.setText("Copy 1");  //Sets the name of the copybutton to" Copy 1"
        copyButton.addActionListener(this);  //Associates the actionlistener with the copybutton GUI component

        copyButtonX.setText("Copy X"); //Sets the name of the copybuttonx that is displayed to "Copy x"
        copyButtonX.addActionListener(this);  //Associates the actionlistener withe the copyx GUI component

        numbercodesText.setColumns(3);  //Sets the width of the numbercodestext textbox to 3 characters
        numbercodesText.setEditable(true);  //Sets the textfield to be editable

        remaining.setColumns(3);  //Sets the width of the remaining text file to 3
        remaining.setEditable(false);  //Sets the textfield to not be editable

        reader history = new reader();  //Creates a new object of the reader class called history
        String lastfile = history.lastfile();  //Uses the object history to declare a value to the string lastfile.  It should be the file last used by the program.  IN the case that the program has never been used before the value will be New

        //Checks if the lastfile string is empty or not
        if(lastfile != null)
        {
            //If the file is not empty it loads the last file
            file = new File(lastfile);
            filelocation.setText(file.getName());  //Sets the text in the textbox to display the file that was loaded

            remaining whatsleft = new remaining();  //Creates a new object in the class remaining

            //Checks the file to see how many product codes are remaining
            try {
                remaining.setText(whatsleft.remaining(file));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(PCDistribGUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(PCDistribGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        setLayout(null);
        add(browseButton);
        add(filelocation);
        add(remaininglabel);
        add(remaining);
        add(copyButton);
        add(X);
        add(numbercodesText);
        add(copyButtonX);
    }

    @Override
    public void actionPerformed(ActionEvent e) //Handles what happens when any of the buttons are pushed
    {
        //handles what happens when the copybutton is pushed
        if (e.getSource() == copyButton && file != null)
        {
            //Trys to do the button
            try 
            {
                int numbercodes = 1; //Sets the number of product codes to 1 since this button only copies one
                reader ProductCodeList1 = new reader(); //Creates a new reader to get the product codes

                String[] activecode = ProductCodeList1.getcodes(numbercodes, file); //Assigns a value to Product Code based on the file

                //Copies the value of product code onto the clipboard
                Copier CopySingle = new Copier();
                CopySingle.copy(activecode);

                //Saves the file again without the used product code
                writer ReSave = new writer();
                ReSave.write(numbercodes, file);

                //Gets the number of product codes that are left in the file
                remaining whatsleft = new remaining();
                try {
                    remaining.setText(whatsleft.remaining(file));
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(PCDistribGUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(PCDistribGUI.class.getName()).log(Level.SEVERE, null, ex);
                }

            } 
            //Handles that exceptions that could occur
            catch (IOException ex) 
            {
                    Logger.getLogger(PCDistribGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //Handles what happens when the copybuttonx is pressed
        else if(e.getSource() == copyButtonX && file != null)
        {
            //Trys to execute the button
            try 
            {
                String numbercodestext = numbercodesText.getText(); //Sets the number of product codes that are being grabbed to the number of codes in the numbercodestext box
                if (!"".equals(numbercodestext)) //Makes sure that the string numbercodestext is not equal to a blank line
                {
                    int numbercodes = Integer.parseInt(numbercodestext); //Converts the string numbercodestext into a integer numbercodes
                    if(numbercodes >= 1) //Checks to make sure that number of codes is a valid number greaterthan or equal to 1
                    {
                        reader ProductCodeList1 = new reader();  //Creates a new reader so that the file can get the product codes

                        String[] activecode = ProductCodeList1.getcodes(numbercodes, file); //Assigns a value to Product Code based on the file

                        //Copies the value of product code onto the clipboard
                        Copier CopySingle = new Copier();
                        CopySingle.copy(activecode);

                        //Saves the file again without the used product code
                        writer ReSave = new writer();
                        ReSave.write(numbercodes, file);

                        //Gets the number of product codes that are left in the file
                        remaining whatsleft = new remaining();
                        try {
                            remaining.setText(whatsleft.remaining(file));
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(PCDistribGUI.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(PCDistribGUI.class.getName()).log(Level.SEVERE, null, ex);
                        }
                     }
                }
            } 
                //Handles the exceptiosn that can occure from a file missing
                catch (IOException ex) 
                {
                        Logger.getLogger(PCDistribGUI.class.getName()).log(Level.SEVERE, null, ex);
                }

        }
        //Handles what happens when the browsebutton is pushed
        else if(e.getSource() == browseButton)
        {
            
            int returnVal = chooser.showOpenDialog(PCDistribGUI.this); //Sets the value of returnVal to set what happens after the button is clicked

            if (returnVal == JFileChooser.APPROVE_OPTION) //If the button is clicked and it is approved this happens
            {
                file = chooser.getSelectedFile(); //Sets file equal to what was selected in the filesector
                filelocation.setText(file.getName()); //Sets the text of the filelocation textbox to the name of the file that was just selected
                //Returns the number of product codes that are remaining and assigns it to the remaining textbox
                remaining whatsleft = new remaining();
                try {
                    remaining.setText(whatsleft.remaining(file));
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(PCDistribGUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(PCDistribGUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            //Saves the file that was selected into the filehistory file
            File recentfile = file; //Sets the value of recentfile to the file that was just opened
            writer filehistorywriter = new writer(); //Creates a new writer object that will write to the filehistory file
            //Writes uses the lastfile method to write recentfile into a string on the textfile to be used when the program is opened again
            try {
                writer.lastfile(recentfile);
            } catch (IOException ex) {
                Logger.getLogger(PCDistribGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    } 
}